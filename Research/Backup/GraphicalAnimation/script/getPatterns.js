
function checkPatternResult(){
    var text = $("#checkPattenText").val();
    var regexInput = $("#checkPatternRegex").val();
    
    if(!checkUndefined(regexInput)){
        var element = regexInput.split('++++');
        if(!checkUndefined(element)){
            alert(getItems(element,text));
        }
    }
    
    
}

function checkRegex(){
    $("#checkPattern").css("display", "block");
}

function closeCheck(){
    $("#checkPattern").css("display", "none");
}


function findPattern()
{
    $("#find-pattern").css("display", "block");
}

function diff(arr1, arr2){
    
    Array.prototype.difference = function(e) {
        return this.filter(function(i) {return e.indexOf(i) < 0;});
    };
    
    if( ((arr1.difference(arr2)).length == 0) && ((arr2.difference(arr1)).length == 0)){
        return false;
    }
    else{
        return true;
    }
    
    
}
function convertArray(arr){
    var result = [];
    var uniqueArr = [];
    for(var i = 0; i< arr.length; i++){
        if(i == 0){
            if(!checkUndefined(arr[0])){
                result.push(arr[0].trim());
                uniqueArr.push(arr[0].trim());
            }
        }
        else{
            var tmp = uniqueArr[uniqueArr.length - 1];
            if((!checkUndefined(arr[i])) && (!checkUndefined(tmp))){
                var a1 = arr[i].trim();
                var a2 = tmp.trim();
                if(a1 != a2){
                    if(result[(result.length - 1)] == "..."){
                        result.push(a2);
                    }
                    
                    result.push(a1);
                    uniqueArr.push(a1);
                }
                else{
                    if(result[(result.length - 1)] != "...")result.push("...");
                }
            }
        }
    }
    if(result[(result.length - 1)] == "..."){
        result.push(uniqueArr[uniqueArr.length - 1]);
    }
    
    return result;
}

function sameArr(arr1, arr2){
    if(!checkUndefined(arr1) && !checkUndefined(arr2)){
        if(arr1.length > 0 && arr2.length > 0){
            //       var a1 = arr1.filter(function(elem, index, self) {
            //                            return index == self.indexOf(elem);
            //                            });
            
            
            var a1 = convertArray(arr1);
            var a2 = convertArray(arr2);
            // compare 2 arrays
            if((a1.length == a2.length) && (a1[0] == a2[0])){
                for(var i = 1; i < a1.length; i++){
                    if(a1[i] != a2[i]){
                        return false;
                    }
                }
                return true;
            }
            //
        }
    }
    
    return false;
    
}
//get the top element in the array.
function topElement(key, arr){
    var result = "";
    var obj = regexPatterns[key];
    if(!checkUndefined(obj["regexLst"])){
        var t = getItems(obj["regexLst"], arr[key]);
        if(t != null) {
            result = t[0].trim();
        }
        
    }
    return result;
}
// get the bottom element in the array
function bottomElement(key, arr){
    var result = "";
    var obj = regexPatterns[key];
    if(!checkUndefined(obj["regexLst"])){
        var t = getItems(regexPatterns[key]["regexLst"], arr[key]);
        if(t != null){
            result = t[(t.length - 1)];
            result = result.trim();
        }
        
    }
    return result;
}

function getElementByIndex(index, key, arr){
    var result = "";
    var obj = regexPatterns[key];
    if(!checkUndefined(obj["regexLst"])){
        var t = getItems(regexPatterns[key]["regexLst"], arr[key]);
        if(t != null && index < t.length){
            result = t[index];
            result = result.trim();
        }
        
    }
    return result;
    
}

function midElement(key, arr){
    var result = "";
    var obj = regexPatterns[key];
    if(!checkUndefined(obj["regexLst"])){
        var t = getItems(regexPatterns[key]["regexLst"], arr[key]);
        if(t != null){
            var index = (t.length - 1)/2;
            result = t[index];
            result = result.trim();
        }
        
    }
    return result;}


// check the condition for the text array which matched regex.
function checkRegexCondition(obj, state, key){
    var result = false;
    
    if(!checkUndefined(obj["compCond"])){
        var str = obj["compCond"]
        var compCondState = (str).replace(/_/g, "key, state") ;
        //var compCondPattern = (str).replace(/_/g, "key, p") ;
        if(compCondState != "" && compCondState != "NONE"){
            var tmpState = state[key];
            state[key] = NormalizeText(key, state[key]);
            
            if(eval(compCondState)) result = true;
            state[key] = tmpState;
            
        }
        else{
            result = true;
        }
        
    }
    return result;
}

// check elements in array matched regex, are the same one which repeated in array
function checkSameRepeatPattern(key, state, p){
    var result = false;
    if(!checkUndefined(regexPatterns[key])){
        var obj = regexPatterns[key];
        if(!checkUndefined(obj["repeat"])){
            var compareOption = obj["repeat"].trim();
            if(compareOption == "REPEAT"){
                // states have elements repeated in one text is collected in one group.
                var eState = getItems(obj["regexLst"], state[key]);
                var ePattern = getItems(obj["regexLst"], p[key]);
                if(sameArr(eState,ePattern)){
                    result = true;
                }
            }
        }
    }
    return result;
}
// check the state existed in the pattern array.
function existPattern(state, index){
    var key, keySize, p, isExits, isSame;
    isExits = false;
    keySize = allKeys.length;
    for (var    i = 0; i < allStatePatterns.length; i++){
        p = allStatePatterns[i]["state"];
        isSame = true;
        for(var j=0; j< keySize; j++){
            key = allKeys[j];
            if(state[key] != p[key]){
                if(!checkSameRepeatPattern(key, state, p)){
                    isSame = false;
                    break;
                }
            }
        }
        if(isSame == true){
            isExits = true;
            break;
        }
    }
    if(isExits == true){
        allStatePatterns[i]["occurs"] = allStatePatterns[i]["occurs"] + 1;
        allStatePatterns[i]["group"].push(index);
        return true;
    }
    return false;
}

function clearPatterns()
{
    $("#statePatterns").html("");
    svgSPIndex = 0;
    
    allStatePatterns = [];
    regexPatterns = [];
    totalStates = 0;
}

function getRegexPatterns(regexInput){
    
//regexPatterns is a global varibale.
    if(!checkUndefined(regexInput)){
        var regexList = regexInput.split('\n');
        for(var i = 0; i< regexList.length; i++){
            var line = regexList[i].split('::::');
            
            if(!checkUndefined(line[1])){
                var regexObj = {};
                regexObj["regexLst"] = line[1].split('++++');
                if(!checkUndefined(line[2]) && line[2] !=""){
                    regexObj["compCond"] = line[2];
                }
                
                if(!checkUndefined(line[3]) && line[3] !=""){
                    regexObj["repeat"] = line[3];
                }
                regexPatterns[line[0].trim()] = regexObj;
            }
        }
    }
}


function selectEnjoyedPtterns(data, conditionInput){
// regexPatterns, allStatePatterns are a global varibales.
    var sizeOfData = data.length;
    var regexKeys = Object.keys(regexPatterns);
    for(var i = 0;i < sizeOfData; i++ ){
        var state = data[i];
        var regexConds = true;
        if(!checkUndefined(regexKeys)){
            for(var j = 0; j< regexKeys.length; j++){
                var key = regexKeys[j];
                if(!checkRegexCondition(regexPatterns[key], state, key)){
                    regexConds = false;
                    break;
                }
            }
        }
        
        if(eval(conditionInput) && regexConds){
            var isExits = existPattern(state, i);
            if(!isExits){
                var sp = {};
               
                sp["state"] = state;
                sp["occurs"] = 1;
                sp["group"] = [i];
                allStatePatterns.push(sp);
            }
        }
    }
    
}

function getPatterns()
{
    $("#statePatterns").html("");
    svgSPIndex = 0;
    totalStates = 0;
    
    allStatePatterns = [];
    regexPatterns = [];
    allOriginalPatterns = [];
    dataForLoadStateGroup = {};
 
    var conditionInput = $("#condition").val().trim();
    if(checkUndefined(conditionInput) || (conditionInput == "")){
        conditionInput = "true";
    }
    
    var regexInput = $("#regex").val().trim();
    var  tmp1, lst, op, cond, tmpCond, state, data, occurs, regexList;
    cond = true; sp = [];
    
    getRegexPatterns(regexInput);

    data = loadData();
    dataForLoadStateGroup = data;
    
    if(lengthLoopStates > 0) data = data.concat(loopStates);
    
    selectEnjoyedPtterns(data, conditionInput);
    
    var sizeOfPatterns = allStatePatterns.length ;
    var total = 0;
    var flag = false;
    
    for( var i = 0;i < sizeOfPatterns; i++ ){
        total = total + allStatePatterns[i]["occurs"];
        
        if(i == (sizeOfPatterns - 1)){
            flag = true;
        }
        drawPattern(data, allStatePatterns[i]["state"], allStatePatterns[i]["occurs"], i, total, flag);

    }
    if(sizeOfPatterns == 0){
        var totalDiv = document.createElement("div");
        totalDiv.setAttribute("class","total");
        var sps = document.getElementById('statePatterns');
        sps.appendChild(totalDiv);
        $(".total").html( "<p>Total: 0 state (0 state pattern)</p> "  );
    }
    
    
                                       
}

function delPattern(index){
    $(".sp" + index).html("");
    $(".sp" + index).remove();
    svgSPIndex = svgSPIndex - 1;
    var linkBottom = ' <a name="top"> </a><a href="#bottom">Go to bottom</a>';
    $(".total").html(linkBottom + " <p>Total: " + totalStates + " states ( " + svgSPIndex + " state patterns)</p>");
}

function definePattern(index)
{
   $("#spForm_" + index).css("display", "block");
}

function cancelPattern(index){
    $("#spForm_" + index).css("display", "none");
}

function gotoState(){
    allData = loadData();
    svg = d3.select("#svgPicture").select("svg");
    
    var index = $("#toState").val();
    var state;
    if(index < lengthData){
        state = allData[index];
    }
    else{
        state = loopStates[index - lengthData];
    }
   
    var numKeys = allKeys.length ;
    var key, sValue, svgText;
    var svgSP;
    var displayState = "" ;
    var initSate = "" ;
    
    
    d3.selectAll(".groups").attr("class","groups hidden");
    
    for (var i= 0; i< numKeys; i++){
        key = allKeys[i];
        sValue = state[key].trim();
        
        displayState = displayState + "#" + key + ": " + sValue  + " ";
        
        svgText = svg.selectAll("[id='" + key.replace("'", "&#39;") + "']" );
        
        if(checkUndefined(svgText) == false && svgText[0].length > 0)
        {
            var tmpValue = texDisplays[key];
            if(!checkUndefined(tmpValue)){
                var option = tmpValue[0];
                
                if(option == "VER-REV" || option == "VER"){
                    NormalizeTextDisplay(key,sValue, "black", 0, 0);
                }
                if(option == "REV"){
                    svgText.text(NormalizeText(key,sValue)).attr("fill", "black");
                }
            }
            else{
                svgText.text(sValue).attr("fill", "black");
            }
        }
        else{ // get groups elements
            var element =  svg.selectAll("[id='" + key.replace("'", "&#39;") + "_" + sValue.replace("'", "&#39;") + "']" );
            if(!checkUndefined(element) && element[0].length > 0){
                element.attr("class", "groups show");
            }
            
        }
    }
    
    d3.select("#diplayStateText").text("State " + (index) + " :" + displayState);
    if(!checkUndefined(index)){
        if(Number(index) > parseInt(index)) stepIndex = parseInt(index) + 1;
        else stepIndex = parseInt(index);
        
        if(stepIndex >= allData.length){
            stepLoopIndex = stepIndex - allData.length;
        }
    }
    
    
}

function closeDisplaying(){
    $("#gotoStateDisplay").html("");

}

function drawState(stateInput, index){
    var numKeys = allKeys.length;
    var key, sValue, svgSP, svgText;
    var state = {};
    var regexInput = $("#defineRegexsp_" + index).val();
    var displayInput = regexInput.split('\n');
    var textPatternDisplays = [];
    if(checkUndefined(displayInput) == false)
    {
        var lenOfDisplayInput = displayInput.length;
        
        for(var i = 0; i < lenOfDisplayInput; i++){
            var element = displayInput[i].split('::::');
            var v = [];
            if(!checkUndefined(element[1]))
            {
                v.push( element[1]);
                v.push( element[2].split('++++')) ;
                textPatternDisplays[element[0]] = v ;
            }
        }
    }
    svgSP = d3.select(".sp" + index).select("svg");
   
    svgSP.selectAll(".groups").attr("class","groups hidden");
   
    for(var j= 0 ; j< numKeys; j++){
        var key = allKeys[j];
        var tmp = (stateInput.replace(/\s+/g," ").split(key + ":"))[1];
        var value;
        if((j + 1) == numKeys ){
            value = tmp.trim();
        }
        else value = tmp.split(allKeys[(j+1)] + ":")[0].trim();
        
        value = formatString(value);
        
        
        svgText =  svgSP.selectAll("[id='" + key.replace("'", "&#39;") + "']" );
        
        if(checkUndefined(svgText) == false && svgText[0].length > 0)
        {
            var tmpValue = textPatternDisplays[key];
            if(checkUndefined(tmpValue) == false)
            {
                var option = tmpValue[0];
                var result = getItems(tmpValue[1], value);
                if(!checkUndefined(result))
                {
                    var t = result.length;
                    if(option == "REV" || option == "VER-REV"){
                        result = result.reverse();
                        var newText = result[0];
                        for( var i = 1 ; i< t; i++){
                            newText = newText + " "  + result[i];
                        }
                        value = newText;
                    }
                }
            }

            svgText.text(value).attr("fill", "black");
        }
        else{ // get groups elements
            var elementsvgText =  svgSP.selectAll("[id='" + key.replace("'", "&#39;") + "_" + value.replace("'", "&#39;") + "']" );
            if(!checkUndefined(elementsvgText) && elementsvgText[0].length > 0){
                elementsvgText.attr("class", "groups show");
            }
            
        }
    }
}

function displayPattern(index){
    var state = $("#definesp_" + index).val();
    drawState(state, index);
    $("#spForm_" + index).css("display", "none");
    
}

function NormalizePatternTextOld(key, text){
    var result = "" ;
    
    if(!checkUndefined(regexPatterns[key])){
        var obj = regexPatterns[key];
        if(!checkUndefined(obj["repeat"])){
            var compareOption = obj["repeat"].trim();
            if(compareOption == "REPEAT"){
                var tmp = getItems(obj["regexLst"], text);
                if(!checkUndefined(tmp)) result = convertArray(tmp);
                //else result = text;
            }
        }
    }
    
    var tmpValue = texDisplays[key];
    
    if(checkUndefined(tmpValue)){
        if(result != "" && !checkUndefined(result)){
            var t = result.length;
            var newText = result[0];
            for( var i = 1 ; i< t; i++){
                newText = newText + " "  + result[i];
            }
            return newText;
        }
        
    }
    // textDisplay is valid
    else{
        var option = tmpValue[0];
        if(result == "" || checkUndefined(result)){
            result = getItems(tmpValue[1], text);
        }
        
        var t = result.length;
        if(option == "REV" || option == "VER-REV"){
            
            result = result.reverse();
            
            var newText = result[0];
            for( var i = 1 ; i< t; i++){
                newText = newText + " "  + result[i];
            }
            return newText;
        }
        
    }
    return text;
}

function NormalizePatternStackText(key, text, svgText){
    var result = "" ;
    
    if(!checkUndefined(regexPatterns[key])){
        var obj = regexPatterns[key];
        if(!checkUndefined(obj["repeat"])){
            var compareOption = obj["repeat"].trim();
            if(compareOption == "REPEAT"){
                var tmp = getItems(obj["regexLst"], text);
                if(!checkUndefined(tmp)) result = convertArray(tmp);
                //else result = text;
            }
        }
    }
    
    var tmpValue = texDisplays[key];
    var option = tmpValue[0];
    if(result == "" || checkUndefined(result)){
        result = getItems(tmpValue[1], text);
    }
    
    var t = result.length;
    if(option == "VER" || option == "VER-REV"){
        if(option == "VER-REV") result = result.reverse();
        var  y = svgText.attr("y");
        //svg1.attr("y", y);
        var  x = svgText.attr("x");
        var lineHeight = 1.3;
        var tspan = svgText.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", lineHeight + "em");
        
        for(var i = 0 ; i< t; i++){
            
            var word = result[i];
            
            tspan.text(word).attr("fill", "black");
            
            lineHeight = (lineHeight - 1*1.0);
            
            tspan = svgText.append("tspan").attr("x", x).attr("y", y).attr("dy",  lineHeight  + "em");
        }
    }
}

// display based on textDisplay first then regexDisplay
function NormalizePatternText(key, text){
    var result = "" ;
    
    var tmpValue = texDisplays[key];
    
    if(checkUndefined(tmpValue)){
        if(!checkUndefined(regexPatterns[key])){
            var obj = regexPatterns[key];
            if(!checkUndefined(obj["repeat"])){
                var compareOption = obj["repeat"].trim();
                if(compareOption == "REPEAT"){
                    var tmp = getItems(obj["regexLst"], text);
                    if(!checkUndefined(tmp)){
                        result = convertArray(tmp);
                    }
                    //else result = text;
                    if(result != "" && !checkUndefined(result)){
                        var t = result.length;
                        var newText = result[0];
                        for( var i = 1 ; i< t; i++){
                            newText = newText + " "  + result[i];
                        }
                        return newText;
                    }
                    else return text;
                }
            }
        }
        
    }
    // textDisplay is valid
    else{
        var option = tmpValue[0];
       
        
        var result = getItems(tmpValue[1], text);
        
        if(!checkUndefined(regexPatterns[key])){
            var obj = regexPatterns[key];
            if(!checkUndefined(obj["repeat"])){
                var compareOption = obj["repeat"].trim();
                if(compareOption == "REPEAT"){
                    if(!checkUndefined(result)){
                        result = convertArray(result);
                    }
                }
            }
        }
        
        var t = result.length;
        if(option == "REV" || option == "VER-REV"){
            
            result = result.reverse();
            var newText = result[0];
            for( var i = 1 ; i< t; i++){
                newText = newText + " "  + result[i];
            }
            return newText;
        }
        
    }
    
    return text;
}

function loadPattern(state){
    var numKeys = allKeys.length ;
    var key, sValue, svgText;
    var svgSP;
    svgSP = d3.select(".sp" + svgSPIndex).select("svg");
   
    
    svgSP.selectAll(".groups").attr("class","groups hidden");
 
    for (var i= 0; i< numKeys; i++){
        key = allKeys[i];
        sValue = state[key].trim();
        
        svgText =  svgSP.selectAll("[id='" + key.replace("'", "&#39;") + "']" );
        
        if(checkUndefined(svgText) == false && svgText[0].length > 0)
        {
            var tmpValue = texDisplays[key];
            if(!checkUndefined(tmpValue)){
                var option = tmpValue[0];
                
                if(option == "VER-REV" || option == "VER"){
                    NormalizePatternStackText(key,sValue,svgText );
                }
                else{
                    svgText.text(NormalizePatternTextOld(key,sValue)).attr("fill", "black");
                }
                
            }
            else{
                svgText.text(NormalizePatternTextOld(key,sValue)).attr("fill", "black");
            }
        }
        else{ // get groups elements
            var element =  svgSP.selectAll("[id='" + key.replace("'", "&#39;") + "_" + sValue.replace("'", "&#39;") + "']" );
            if(!checkUndefined(element) && element[0].length > 0){
                element.attr("class", "groups show");
            }
            
        }
    }
    
}

function drawPattern(data, state, occurs, index, total,flag){
    
    var sps = document.getElementById('statePatterns');
    svgSPIndex = svgSPIndex + 1;
    var ele = document.createElement("div");
    ele.setAttribute("class","spdiv sp" + svgSPIndex);
    sps.appendChild(ele);
   
    var delButton= '<input type="button" class="btn btn-default btn-xs" onclick="delPattern(' + svgSPIndex + ')" value="Delete Pattern" style="margin-left:2px" />';
    
    var setButton= '<input type="button" class="btn btn-default btn-xs" onclick="definePattern(' + svgSPIndex + ')" value="Define Pattern" style="margin-left:2px" />';

    var htmlForm = '<div style = "display: none" id="spForm_'
                    + svgSPIndex + '" >State Pattern:<textarea id="definesp_'
                    + svgSPIndex + '" rows="4" cols="60"> </textarea>'
    + 'Displaying Regex: <textarea id="defineRegexsp_' + svgSPIndex + '" rows="4" cols="50"> </textarea>'
        + '<input type="button" class="btn btn-default btn-xs" onclick="displayPattern('
        + svgSPIndex +')" value="Display" style="margin-left:2px;margin-bottom:15px"/>'
        +  '<input type="button" class="btn btn-default btn-xs" onclick="cancelPattern('
        + svgSPIndex +')" value="Cancel" style="margin-left:2px;margin-bottom:15px"/></div>';
   
    var htmlGroup = '<p id="groupsp_' + index + '" ></p>'   ;
    
    $(".sp" + svgSPIndex).html("<p>State Pattern " + svgSPIndex + " (<a href='javascript:loadGroup(" + index + ")'>" + occurs + " states</a>) " + setButton + delButton + "</p>" + htmlGroup + htmlForm +  svgPicture);
    
    
    
    loadPattern(state);
    
    
    
    if(flag){
        var linkTop = '<a href="#top">Go to top</a><a name="bottom"> </a>';
        var linkBottom = ' <a name="top"> </a><a href="#bottom">Go to bottom</a>';
        
        var bottomDiv = document.createElement("div");
        bottomDiv.setAttribute("class","bottomDiv");
        sps.appendChild(bottomDiv);
        $(".bottomDiv" ).html(linkTop);
        
        totalStates = total;
        var totalDiv = document.createElement("div");
        totalDiv.setAttribute("class","total");
       
        sps.prepend(totalDiv);
         $(".total").html(linkBottom + "<p>Total: " + total + " states ( " + svgSPIndex + " state patterns)</p> "  );
    }

}

function loadGroup( index){
    
   // dataForLoadStateGroup
    
    var group = allStatePatterns[index]["group"];
    var size = group.length;
    var str = "State: ";
    for(var i = 0; i< size; i++){
        if(i == size - 1){
            str = str + group[i].toString();
        }
        else str = str + group[i].toString() + ",";
    }
    
    $("#groupsp_" + index).html(str);
     $("#groupsp_" + index).text(str);
   // alert(str);
}

