
###keys
chan1 chan2 bit1 bit2 pac list

###textDisplay
chan1::::REV::::<_,_>++++empty

###states
(chan1: empty chan2: empty bit1: false bit2: false pac: pac(0) list: nil) ||
(chan1: empty chan2: (false empty) bit1: false bit2: false pac: pac(0) list: nil) ||
(chan1: (< false,pac(0) > empty) chan2: (false empty) bit1: false bit2: false pac: pac(0) list: nil) ||
(chan1: (< false,pac(0) > < false,pac(0) > empty) chan2: (false empty) bit1: false bit2: false pac: pac(0) list: nil)

###loop
(chan1: (< false,pac(0) > empty) chan2: (false empty) bit1: false bit2: false pac: pac(0) list: nil) ||
(chan1: empty chan2: (false empty) bit1: false bit2: false pac: pac(0) list: nil)