--> I) Base case
open INV .
  red inv1(init) .
close

--> II) Inductive cese
--> 1) send1(s)
open ISTEP .
-- arbitrary values
-- assumptions
-- successor state
  eq s' = send1(s) .
-- check
  red istep1 .
close

--> 2) rec1(s)
--> c-rec1(s), bit1(s) = b
open ISTEP .
-- arbitrary values
  op b : -> Bool .
  op bs : -> BFifo .
-- assumptions
  -- eq c-rec1(s) = true .
  eq fifo2(s) = b,bs .
--
  eq bit1(s) = b .
-- successor state
  eq s' = rec1(s) .
-- check
  red istep1 .
close
--> c-rec1(s), ~(bit1(s) = b), bit2(s) = b
open ISTEP .
-- arbitrary values
  op b : -> Bool .
  op bs : -> BFifo .
-- assumptions
  -- eq c-rec1(s) = true .
  eq fifo2(s) = b,bs .
--
  eq (bit1(s) = b) = false .
  eq bit2(s) = b .
-- successor state
  eq s' = rec1(s) .
-- check
  red istep1 .
close
--> c-rec1(s), ~(bit1(s) = b), ~(bit2(s) = b)

open ISTEP .
-- arbitrary values
  op b : -> Bool .
  op bs : -> BFifo .
-- assumptions
  -- eq c-rec1(s) = true .
  eq fifo2(s) = b,bs .
--
  eq (bit1(s) = b) = false .
  eq (bit2(s) = b) = false .
-- successor state
  eq s' = rec1(s) .
-- check
  red  inv2(s) implies istep1 .
close
--> ~c-rec1(s)



open ISTEP .
-- arbitrary values
-- assumptions
  eq c-rec1(s) = false .
-- successor state
  eq s' = rec1(s) .
-- check
  red istep1 .
close

--> 3) send2(s)
open ISTEP .
-- arbitrary values
-- assumptions
-- successor state
  eq s' = send2(s) .
-- check
  red istep1 .
close

--> 4) rec2(s)
--> c-rec2(s), ~(bit2(s) = fst(p))
open ISTEP .
-- arbitrary values
  op p : -> BPPair .
  op ps : -> PFifo .
-- assumptions
  -- eq c-rec2(s) = true .
  eq fifo1(s) = p,ps .
--
  eq (bit2(s) = fst(p)) = false .
-- successor state
  eq s' = rec2(s) .
-- check
  red istep1 .
close
--> c-rec2(s), bit2(s) = fst(p), ~(pac(next(s)) = snd(p))
open ISTEP .
-- arbitrary values
  op p : -> BPPair .
  op ps : -> PFifo .
-- assumptions
  -- eq c-rec2(s) = true .
  eq fifo1(s) = p,ps .
--
  eq bit2(s) = fst(p) .
  eq (pac(next(s)) = snd(p)) = false .
-- successor state
  eq s' = rec2(s) .
-- check
  red  istep1 .
close

eof .

--> c-rec2(s), bit2(s) = fst(p), pac(next(s)) = snd(p), 
--> bit1(s) = fst(p), ~(fst(p) = (fst(p) xor true))
open ISTEP .
-- arbitrary values
  op p : -> BPPair .
  op ps : -> PFifo .
-- assumptions
  -- eq c-rec2(s) = true .
  eq fifo1(s) = p,ps .
--
  eq bit2(s) = fst(p) .
  eq pac(next(s)) = snd(p) .
  eq bit1(s) = fst(p) .
  eq (fst(p) = (fst(p) xor true)) = false .
-- successor state
  eq s' = rec2(s) .
-- check
  red istep1 .
close
--> c-rec2(s), bit2(s) = fst(p), pac(next(s)) = snd(p), 
--> bit1(s) = fst(p), fst(p) = (fst(p) xor true)
open ISTEP .
-- arbitrary values
  op p : -> BPPair .
  op ps : -> PFifo .
-- assumptions
  -- eq c-rec2(s) = true .
  eq fifo1(s) = p,ps .
--
  eq bit2(s) = fst(p) .
  eq pac(next(s)) = snd(p) .
  eq bit1(s) = fst(p) .
  eq (fst(p) = (fst(p) xor true)) = true .
-- successor state
  eq s' = rec2(s) .
-- check
  red eqbool-lemma1(fst(p)) implies istep1 .
close
--> c-rec2(s), bit2(s) = fst(p), pac(next(s)) = snd(p), 
--> ~(bit1(s) = fst(p))
open ISTEP .
-- arbitrary values
  op p : -> BPPair .
  op ps : -> PFifo .
-- assumptions
  -- eq c-rec2(s) = true .
  eq fifo1(s) = p,ps .
--
  eq bit2(s) = fst(p) .
  eq pac(next(s)) = snd(p) .
  eq (bit1(s) = fst(p)) = false .
-- successor state
  eq s' = rec2(s) .
-- check
  red inv3(s) implies istep1 .
close
--> ~c-rec2(s)
open ISTEP .
-- arbitrary values
-- assumptions
  eq c-rec2(s) = false .
-- successor state
  eq s' = rec2(s) .
-- check
  red istep1 .
close

--> 5) drop1(s)
--> c-drop1(s)
open ISTEP .
-- arbitrary values
  op p : -> BPPair .
  op ps : -> PFifo .
-- assumptions
  -- eq c-drop1(s) = true .
  eq fifo1(s) = p,ps .
-- successor state
  eq s' = drop1(s) .
-- check
  red istep1 .
close
--> ~c-drop1(s)
open ISTEP .
-- arbitrary values
-- assumptions
  eq c-drop1(s) = false .
-- successor state
  eq s' = drop1(s) .
-- check
  red istep1 .
close

--> 6) dup1(s)
--> c-dup1(s)
open ISTEP .
-- arbitrary values
  op p : -> BPPair .
  op ps : -> PFifo .
-- assumptions
  -- eq c-dup1(s) = true .
  eq fifo1(s) = p,ps .
-- successor state
  eq s' = dup1(s) .
-- check
  red istep1 .
close
--> ~c-dup1(s)
open ISTEP .
-- arbitrary values
-- assumptions
  eq c-dup1(s) = false .
-- successor state
  eq s' = dup1(s) .
-- check
  red istep1 .
close

--> 7) drop2(s)
--> c-drop2(s)
open ISTEP .
-- arbitrary values
  op b : -> Bool .
  op bs : -> BFifo .
-- assumptions
  -- eq c-drop2(s) = true .
  eq fifo2(s) = b,bs .
-- successor state
  eq s' = drop2(s) .
-- check
  red istep1 .
close
--> ~c-drop2(s)
open ISTEP .
-- arbitrary values
-- assumptions
  eq c-drop2(s) = false .
-- successor state
  eq s' = drop2(s) .
-- check
  red istep1 .
close

--> 8) dup2(s)
--> c-dup2(s)
open ISTEP .
-- arbitrary values
  op b : -> Bool .
  op bs : -> BFifo .
-- assumptions
  -- eq c-dup2(s) = true .
  eq fifo2(s) = b,bs .
-- successor state
  eq s' = dup2(s) .
-- check
  red istep1 .
close
--> ~c-dup2(s)
open ISTEP .
-- arbitrary values
-- assumptions
  eq c-dup2(s) = false .
-- successor state
  eq s' = dup2(s) .
-- check
  red istep1 .
close

--> QED


